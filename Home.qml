/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5

Page {

    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">LAMB</font>")
        } else {
            qsTr("<font color=\"dimgrey\">LAMB</font>")

        }
    }

    Image {
        id: image
        x: 105
        y: 106
        anchors.fill: parent
        source: "qrc:files/images/LAMB_Cover.jpg"
        fillMode: Image.PreserveAspectFit
    }

}



/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
