/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebooknum
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Numbers</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Numbers</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrollnum + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Numbers.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Numbers.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavnum = true
                    settings.booklocnum = "#" + model.get(index).value
                    settings.finalurlnum = "qrc:files/bible/NUM.xhtml" + settings.booklocnum                   
                    webview.url = settings.finalurlnum    
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrollnum: 0
        property string booklocnum: ""
        property string finalurlnum: ""
        property bool isusernavnum: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/NUM.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollnum = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavnum == false) {
                    runJavaScript(biblebooknum.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavnum = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Numbers 1"; value: "NU1_0";}
        ListElement { text: "Numbers 2"; value: "NU2_0";}
        ListElement { text: "Numbers 3"; value: "NU3_0";}
        ListElement { text: "Numbers 4"; value: "NU4_0";}
        ListElement { text: "Numbers 5"; value: "NU5_0";}
        ListElement { text: "Numbers 6"; value: "NU6_0";}
        ListElement { text: "Numbers 7"; value: "NU7_0";}
        ListElement { text: "Numbers 8"; value: "NU8_0";}
        ListElement { text: "Numbers 9"; value: "NU9_0";}
        ListElement { text: "Numbers 10"; value: "NU10_0";}
        ListElement { text: "Numbers 11"; value: "NU11_0";}
        ListElement { text: "Numbers 12"; value: "NU12_0";}
        ListElement { text: "Numbers 13"; value: "NU13_0";}
        ListElement { text: "Numbers 14"; value: "NU14_0";}
        ListElement { text: "Numbers 15"; value: "NU15_0";}
        ListElement { text: "Numbers 16"; value: "NU16_0";}
        ListElement { text: "Numbers 17"; value: "NU17_0";}
        ListElement { text: "Numbers 18"; value: "NU18_0";}
        ListElement { text: "Numbers 19"; value: "NU19_0";}
        ListElement { text: "Numbers 20"; value: "NU20_0";}
        ListElement { text: "Numbers 21"; value: "NU21_0";}
        ListElement { text: "Numbers 22"; value: "NU22_0";}
        ListElement { text: "Numbers 23"; value: "NU23_0";}
        ListElement { text: "Numbers 24"; value: "NU24_0";}
        ListElement { text: "Numbers 25"; value: "NU25_0";}
        ListElement { text: "Numbers 26"; value: "NU26_0";}
        ListElement { text: "Numbers 27"; value: "NU27_0";}
        ListElement { text: "Numbers 28"; value: "NU28_0";}
        ListElement { text: "Numbers 29"; value: "NU29_0";}
        ListElement { text: "Numbers 30"; value: "NU30_0";}
        ListElement { text: "Numbers 31"; value: "NU31_0";}
        ListElement { text: "Numbers 32"; value: "NU32_0";}
        ListElement { text: "Numbers 33"; value: "NU33_0";}
        ListElement { text: "Numbers 34"; value: "NU34_0";}
        ListElement { text: "Numbers 35"; value: "NU35_0";}
        ListElement { text: "Numbers 36"; value: "NU36_0";}
    }
}
