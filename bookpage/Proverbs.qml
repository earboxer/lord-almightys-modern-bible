/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookpro
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Proverbs</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Proverbs</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrollpro + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Proverbs.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Proverbs.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavpro = true
                    settings.booklocpro = "#" + model.get(index).value
                    settings.finalurlpro = "qrc:files/bible/PRO.xhtml" + settings.booklocpro
                    webview.url = settings.finalurlpro
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrollpro: 0
        property string booklocpro: ""
        property string finalurlpro: ""
        property bool isusernavpro: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/PRO.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollpro = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavpro == false) {
                    runJavaScript(biblebookpro.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavpro = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Proverbs 1"; value: "PR1_0";}
        ListElement { text: "Proverbs 2"; value: "PR2_0";}
        ListElement { text: "Proverbs 3"; value: "PR3_0";}
        ListElement { text: "Proverbs 4"; value: "PR4_0";}
        ListElement { text: "Proverbs 5"; value: "PR5_0";}
        ListElement { text: "Proverbs 6"; value: "PR6_0";}
        ListElement { text: "Proverbs 7"; value: "PR7_0";}
        ListElement { text: "Proverbs 8"; value: "PR8_0";}
        ListElement { text: "Proverbs 9"; value: "PR9_0";}
        ListElement { text: "Proverbs 10"; value: "PR10_0";}
        ListElement { text: "Proverbs 11"; value: "PR11_0";}
        ListElement { text: "Proverbs 12"; value: "PR12_0";}
        ListElement { text: "Proverbs 13"; value: "PR13_0";}
        ListElement { text: "Proverbs 14"; value: "PR14_0";}
        ListElement { text: "Proverbs 15"; value: "PR15_0";}
        ListElement { text: "Proverbs 16"; value: "PR16_0";}
        ListElement { text: "Proverbs 17"; value: "PR17_0";}
        ListElement { text: "Proverbs 18"; value: "PR18_0";}
        ListElement { text: "Proverbs 19"; value: "PR19_0";}
        ListElement { text: "Proverbs 20"; value: "PR20_0";}
        ListElement { text: "Proverbs 21"; value: "PR21_0";}
        ListElement { text: "Proverbs 22"; value: "PR22_0";}
        ListElement { text: "Proverbs 23"; value: "PR23_0";}
        ListElement { text: "Proverbs 24"; value: "PR24_0";}
        ListElement { text: "Proverbs 25"; value: "PR25_0";}
        ListElement { text: "Proverbs 26"; value: "PR26_0";}
        ListElement { text: "Proverbs 27"; value: "PR27_0";}
        ListElement { text: "Proverbs 28"; value: "PR28_0";}
        ListElement { text: "Proverbs 29"; value: "PR29_0";}
        ListElement { text: "Proverbs 30"; value: "PR30_0";}
        ListElement { text: "Proverbs 31"; value: "PR31_0";}
    }
}
