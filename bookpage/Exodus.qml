/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookexo
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Exodus</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Exodus</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrollexo + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Exodus.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Exodus.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavexo = true
                    settings.booklocexo = "#" + model.get(index).value
                    settings.finalurlexo = "qrc:files/bible/EXO.xhtml" + settings.booklocexo
                    webview.url = settings.finalurlexo
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrollexo: 0
        property string booklocexo: ""
        property string finalurlexo: ""
        property bool isusernavexo: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/EXO.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollexo = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavexo == false) {
                    runJavaScript(biblebookexo.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavexo = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Exodus 1"; value: "EX1_0";}
        ListElement { text: "Exodus 2"; value: "EX2_0";}
        ListElement { text: "Exodus 3"; value: "EX3_0";}
        ListElement { text: "Exodus 4"; value: "EX4_0";}
        ListElement { text: "Exodus 5"; value: "EX5_0";}
        ListElement { text: "Exodus 6"; value: "EX6_0";}
        ListElement { text: "Exodus 7"; value: "EX7_0";}
        ListElement { text: "Exodus 8"; value: "EX8_0";}
        ListElement { text: "Exodus 9"; value: "EX9_0";}
        ListElement { text: "Exodus 10"; value: "EX10_0";}
        ListElement { text: "Exodus 11"; value: "EX11_0";}
        ListElement { text: "Exodus 12"; value: "EX12_0";}
        ListElement { text: "Exodus 13"; value: "EX13_0";}
        ListElement { text: "Exodus 14"; value: "EX14_0";}
        ListElement { text: "Exodus 15"; value: "EX15_0";}
        ListElement { text: "Exodus 16"; value: "EX16_0";}
        ListElement { text: "Exodus 17"; value: "EX17_0";}
        ListElement { text: "Exodus 18"; value: "EX18_0";}
        ListElement { text: "Exodus 19"; value: "EX19_0";}
        ListElement { text: "Exodus 20"; value: "EX20_0";}
        ListElement { text: "Exodus 21"; value: "EX21_0";}
        ListElement { text: "Exodus 22"; value: "EX22_0";}
        ListElement { text: "Exodus 23"; value: "EX23_0";}
        ListElement { text: "Exodus 24"; value: "EX24_0";}
        ListElement { text: "Exodus 25"; value: "EX25_0";}
        ListElement { text: "Exodus 26"; value: "EX26_0";}
        ListElement { text: "Exodus 27"; value: "EX27_0";}
        ListElement { text: "Exodus 28"; value: "EX28_0";}
        ListElement { text: "Exodus 29"; value: "EX29_0";}
        ListElement { text: "Exodus 30"; value: "EX30_0";}
        ListElement { text: "Exodus 31"; value: "EX31_0";}
        ListElement { text: "Exodus 32"; value: "EX32_0";}
        ListElement { text: "Exodus 33"; value: "EX33_0";}
        ListElement { text: "Exodus 34"; value: "EX34_0";}
        ListElement { text: "Exodus 35"; value: "EX35_0";}
        ListElement { text: "Exodus 36"; value: "EX36_0";}
        ListElement { text: "Exodus 37"; value: "EX37_0";}
        ListElement { text: "Exodus 38"; value: "EX38_0";}
        ListElement { text: "Exodus 39"; value: "EX39_0";}
        ListElement { text: "Exodus 40"; value: "EX40_0";}

    }
}
