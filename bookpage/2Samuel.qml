/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebook2sa
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">2 Samuel</font>")

        } else {
            qsTr("<font color=\"dimgrey\">2 Samuel</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescroll2sa + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("2Samuel.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("2Samuel.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernav2sa = true
                    settings.bookloc2sa = "#" + model.get(index).value
                    settings.finalurl2sa = "qrc:files/bible/2SA.xhtml" + settings.bookloc2sa
                    webview.url = settings.finalurl2sa
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescroll2sa: 0
        property string bookloc2sa: ""
        property string finalurl2sa: ""
        property bool isusernav2sa: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/2SA.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll2sa = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernav2sa == false) {
                    runJavaScript(biblebook2sa.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernav2sa = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "2 Samuel 1"; value: "S21_0";}
        ListElement { text: "2 Samuel 2"; value: "S22_0";}
        ListElement { text: "2 Samuel 3"; value: "S23_0";}
        ListElement { text: "2 Samuel 4"; value: "S24_0";}
        ListElement { text: "2 Samuel 5"; value: "S25_0";}
        ListElement { text: "2 Samuel 6"; value: "S26_0";}
        ListElement { text: "2 Samuel 7"; value: "S27_0";}
        ListElement { text: "2 Samuel 8"; value: "S28_0";}
        ListElement { text: "2 Samuel 9"; value: "S29_0";}
        ListElement { text: "2 Samuel 10"; value: "S210_0";}
        ListElement { text: "2 Samuel 11"; value: "S211_0";}
        ListElement { text: "2 Samuel 12"; value: "S212_0";}
        ListElement { text: "2 Samuel 13"; value: "S213_0";}
        ListElement { text: "2 Samuel 14"; value: "S214_0";}
        ListElement { text: "2 Samuel 15"; value: "S215_0";}
        ListElement { text: "2 Samuel 16"; value: "S216_0";}
        ListElement { text: "2 Samuel 17"; value: "S217_0";}
        ListElement { text: "2 Samuel 18"; value: "S218_0";}
        ListElement { text: "2 Samuel 19"; value: "S219_0";}
        ListElement { text: "2 Samuel 20"; value: "S220_0";}
        ListElement { text: "2 Samuel 21"; value: "S221_0";}
        ListElement { text: "2 Samuel 22"; value: "S222_0";}
        ListElement { text: "2 Samuel 23"; value: "S223_0";}
        ListElement { text: "2 Samuel 24"; value: "S224_0";}
    }
}
