/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebook2ch
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">2 Chronicles</font>")

        } else {
            qsTr("<font color=\"dimgrey\">2 Chronicles</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescroll2ch + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("2Chronicles.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("2Chronicles.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernav2ch = true
                    settings.bookloc2ch = "#" + model.get(index).value
                    settings.finalurl2ch = "qrc:files/bible/2CH.xhtml" + settings.bookloc2ch
                    webview.url = settings.finalurl2ch
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescroll2ch: 0
        property string bookloc2ch: ""
        property string finalurl2ch: ""
        property bool isusernav2ch: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/2CH.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll2ch = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernav2ch == false) {
                    runJavaScript(biblebook2ch.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernav2ch = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "2 Chronicles 1"; value: "R21_0";}
        ListElement { text: "2 Chronicles 2"; value: "R22_0";}
        ListElement { text: "2 Chronicles 3"; value: "R23_0";}
        ListElement { text: "2 Chronicles 4"; value: "R24_0";}
        ListElement { text: "2 Chronicles 5"; value: "R25_0";}
        ListElement { text: "2 Chronicles 6"; value: "R26_0";}
        ListElement { text: "2 Chronicles 7"; value: "R27_0";}
        ListElement { text: "2 Chronicles 8"; value: "R28_0";}
        ListElement { text: "2 Chronicles 9"; value: "R29_0";}
        ListElement { text: "2 Chronicles 10"; value: "R210_0";}
        ListElement { text: "2 Chronicles 11"; value: "R211_0";}
        ListElement { text: "2 Chronicles 12"; value: "R212_0";}
        ListElement { text: "2 Chronicles 13"; value: "R213_0";}
        ListElement { text: "2 Chronicles 14"; value: "R214_0";}
        ListElement { text: "2 Chronicles 15"; value: "R215_0";}
        ListElement { text: "2 Chronicles 16"; value: "R216_0";}
        ListElement { text: "2 Chronicles 17"; value: "R217_0";}
        ListElement { text: "2 Chronicles 18"; value: "R218_0";}
        ListElement { text: "2 Chronicles 19"; value: "R219_0";}
        ListElement { text: "2 Chronicles 20"; value: "R220_0";}
        ListElement { text: "2 Chronicles 21"; value: "R221_0";}
        ListElement { text: "2 Chronicles 22"; value: "R222_0";}
        ListElement { text: "2 Chronicles 23"; value: "R223_0";}
        ListElement { text: "2 Chronicles 24"; value: "R224_0";}
        ListElement { text: "2 Chronicles 25"; value: "R225_0";}
        ListElement { text: "2 Chronicles 26"; value: "R226_0";}
        ListElement { text: "2 Chronicles 27"; value: "R227_0";}
        ListElement { text: "2 Chronicles 28"; value: "R228_0";}
        ListElement { text: "2 Chronicles 29"; value: "R229_0";}
        ListElement { text: "2 Chronicles 30"; value: "R230_0";}
        ListElement { text: "2 Chronicles 31"; value: "R231_0";}
        ListElement { text: "2 Chronicles 32"; value: "R232_0";}
        ListElement { text: "2 Chronicles 33"; value: "R233_0";}
        ListElement { text: "2 Chronicles 34"; value: "R234_0";}
        ListElement { text: "2 Chronicles 35"; value: "R235_0";}
        ListElement { text: "2 Chronicles 36"; value: "R236_0";}
    }
}
