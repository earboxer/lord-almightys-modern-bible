/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebook1ki
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">1 Kings</font>")

        } else {
            qsTr("<font color=\"dimgrey\">1 Kings</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescroll1ki + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("1Kings.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("1Kings.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernav1ki = true
                    settings.bookloc1ki = "#" + model.get(index).value
                    settings.finalurl1ki = "qrc:files/bible/1KI.xhtml" + settings.bookloc1ki
                    webview.url = settings.finalurl1ki
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescroll1ki: 0
        property string bookloc1ki: ""
        property string finalurl1ki: ""
        property bool isusernav1ki: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/1KI.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll1ki = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernav1ki == false) {
                    runJavaScript(biblebook1ki.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernav1ki = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "1 Kings 1"; value: "K11_0";}
        ListElement { text: "1 Kings 2"; value: "K12_0";}
        ListElement { text: "1 Kings 3"; value: "K13_0";}
        ListElement { text: "1 Kings 4"; value: "K14_0";}
        ListElement { text: "1 Kings 5"; value: "K15_0";}
        ListElement { text: "1 Kings 6"; value: "K16_0";}
        ListElement { text: "1 Kings 7"; value: "K17_0";}
        ListElement { text: "1 Kings 8"; value: "K18_0";}
        ListElement { text: "1 Kings 9"; value: "K19_0";}
        ListElement { text: "1 Kings 10"; value: "K110_0";}
        ListElement { text: "1 Kings 11"; value: "K111_0";}
        ListElement { text: "1 Kings 12"; value: "K112_0";}
        ListElement { text: "1 Kings 13"; value: "K113_0";}
        ListElement { text: "1 Kings 14"; value: "K114_0";}
        ListElement { text: "1 Kings 15"; value: "K115_0";}
        ListElement { text: "1 Kings 16"; value: "K116_0";}
        ListElement { text: "1 Kings 17"; value: "K117_0";}
        ListElement { text: "1 Kings 18"; value: "K118_0";}
        ListElement { text: "1 Kings 19"; value: "K119_0";}
        ListElement { text: "1 Kings 20"; value: "K120_0";}
        ListElement { text: "1 Kings 21"; value: "K121_0";}
        ListElement { text: "1 Kings 22"; value: "K122_0";}
    }
}
