/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookisa
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Isaiah</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Isaiah</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrollisa + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Isaiah.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Isaiah.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavisa = true
                    settings.booklocisa = "#" + model.get(index).value
                    settings.finalurlisa = "qrc:files/bible/ISA.xhtml" + settings.booklocisa
                    webview.url = settings.finalurlisa
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrollisa: 0
        property string booklocisa: ""
        property string finalurlisa: ""
        property bool isusernavisa: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/ISA.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollisa = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavisa == false) {
                    runJavaScript(biblebookisa.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavisa = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Isaiah 1"; value: "IS1_0";}
        ListElement { text: "Isaiah 2"; value: "IS2_0";}
        ListElement { text: "Isaiah 3"; value: "IS3_0";}
        ListElement { text: "Isaiah 4"; value: "IS4_0";}
        ListElement { text: "Isaiah 5"; value: "IS5_0";}
        ListElement { text: "Isaiah 6"; value: "IS6_0";}
        ListElement { text: "Isaiah 7"; value: "IS7_0";}
        ListElement { text: "Isaiah 8"; value: "IS8_0";}
        ListElement { text: "Isaiah 9"; value: "IS9_0";}
        ListElement { text: "Isaiah 10"; value: "IS10_0";}
        ListElement { text: "Isaiah 11"; value: "IS11_0";}
        ListElement { text: "Isaiah 12"; value: "IS12_0";}
        ListElement { text: "Isaiah 13"; value: "IS13_0";}
        ListElement { text: "Isaiah 14"; value: "IS14_0";}
        ListElement { text: "Isaiah 15"; value: "IS15_0";}
        ListElement { text: "Isaiah 16"; value: "IS16_0";}
        ListElement { text: "Isaiah 17"; value: "IS17_0";}
        ListElement { text: "Isaiah 18"; value: "IS18_0";}
        ListElement { text: "Isaiah 19"; value: "IS19_0";}
        ListElement { text: "Isaiah 20"; value: "IS20_0";}
        ListElement { text: "Isaiah 21"; value: "IS21_0";}
        ListElement { text: "Isaiah 22"; value: "IS22_0";}
        ListElement { text: "Isaiah 23"; value: "IS23_0";}
        ListElement { text: "Isaiah 24"; value: "IS24_0";}
        ListElement { text: "Isaiah 25"; value: "IS25_0";}
        ListElement { text: "Isaiah 26"; value: "IS26_0";}
        ListElement { text: "Isaiah 27"; value: "IS27_0";}
        ListElement { text: "Isaiah 28"; value: "IS28_0";}
        ListElement { text: "Isaiah 29"; value: "IS29_0";}
        ListElement { text: "Isaiah 30"; value: "IS30_0";}
        ListElement { text: "Isaiah 31"; value: "IS31_0";}
        ListElement { text: "Isaiah 32"; value: "IS32_0";}
        ListElement { text: "Isaiah 33"; value: "IS33_0";}
        ListElement { text: "Isaiah 34"; value: "IS34_0";}
        ListElement { text: "Isaiah 35"; value: "IS35_0";}
        ListElement { text: "Isaiah 36"; value: "IS36_0";}
        ListElement { text: "Isaiah 37"; value: "IS37_0";}
        ListElement { text: "Isaiah 38"; value: "IS38_0";}
        ListElement { text: "Isaiah 39"; value: "IS39_0";}
        ListElement { text: "Isaiah 40"; value: "IS40_0";}
        ListElement { text: "Isaiah 41"; value: "IS41_0";}
        ListElement { text: "Isaiah 42"; value: "IS42_0";}
        ListElement { text: "Isaiah 43"; value: "IS43_0";}
        ListElement { text: "Isaiah 44"; value: "IS44_0";}
        ListElement { text: "Isaiah 45"; value: "IS45_0";}
        ListElement { text: "Isaiah 46"; value: "IS46_0";}
        ListElement { text: "Isaiah 47"; value: "IS47_0";}
        ListElement { text: "Isaiah 48"; value: "IS48_0";}
        ListElement { text: "Isaiah 49"; value: "IS49_0";}
        ListElement { text: "Isaiah 50"; value: "IS50_0";}
        ListElement { text: "Isaiah 51"; value: "IS51_0";}
        ListElement { text: "Isaiah 52"; value: "IS52_0";}
        ListElement { text: "Isaiah 53"; value: "IS53_0";}
        ListElement { text: "Isaiah 54"; value: "IS54_0";}
        ListElement { text: "Isaiah 55"; value: "IS55_0";}
        ListElement { text: "Isaiah 56"; value: "IS56_0";}
        ListElement { text: "Isaiah 57"; value: "IS57_0";}
        ListElement { text: "Isaiah 58"; value: "IS58_0";}
        ListElement { text: "Isaiah 59"; value: "IS59_0";}
        ListElement { text: "Isaiah 60"; value: "IS60_0";}
        ListElement { text: "Isaiah 61"; value: "IS61_0";}
        ListElement { text: "Isaiah 62"; value: "IS62_0";}
        ListElement { text: "Isaiah 63"; value: "IS63_0";}
        ListElement { text: "Isaiah 64"; value: "IS64_0";}
        ListElement { text: "Isaiah 65"; value: "IS65_0";}
        ListElement { text: "Isaiah 66"; value: "IS66_0";}
    }
}
