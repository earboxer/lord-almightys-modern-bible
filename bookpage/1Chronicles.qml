/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebook1ch
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">1 Chronicles</font>")

        } else {
            qsTr("<font color=\"dimgrey\">1 Chronicles</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescroll1ch + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("1Chronicles.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("1Chronicles.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernav1ch = true
                    settings.bookloc1ch = "#" + model.get(index).value
                    settings.finalurl1ch = "qrc:files/bible/1CH.xhtml" + settings.bookloc1ch
                    webview.url = settings.finalurl1ch
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescroll1ch: 0
        property string bookloc1ch: ""
        property string finalurl1ch: ""
        property bool isusernav1ch: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/1CH.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll1ch = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernav1ch == false) {
                    runJavaScript(biblebook1ch.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernav1ch = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "1 Chronicles 1"; value: "R11_0";}
        ListElement { text: "1 Chronicles 2"; value: "R12_0";}
        ListElement { text: "1 Chronicles 3"; value: "R13_0";}
        ListElement { text: "1 Chronicles 4"; value: "R14_0";}
        ListElement { text: "1 Chronicles 5"; value: "R15_0";}
        ListElement { text: "1 Chronicles 6"; value: "R16_0";}
        ListElement { text: "1 Chronicles 7"; value: "R17_0";}
        ListElement { text: "1 Chronicles 8"; value: "R18_0";}
        ListElement { text: "1 Chronicles 9"; value: "R19_0";}
        ListElement { text: "1 Chronicles 10"; value: "R110_0";}
        ListElement { text: "1 Chronicles 11"; value: "R111_0";}
        ListElement { text: "1 Chronicles 12"; value: "R112_0";}
        ListElement { text: "1 Chronicles 13"; value: "R113_0";}
        ListElement { text: "1 Chronicles 14"; value: "R114_0";}
        ListElement { text: "1 Chronicles 15"; value: "R115_0";}
        ListElement { text: "1 Chronicles 16"; value: "R116_0";}
        ListElement { text: "1 Chronicles 17"; value: "R117_0";}
        ListElement { text: "1 Chronicles 18"; value: "R118_0";}
        ListElement { text: "1 Chronicles 19"; value: "R119_0";}
        ListElement { text: "1 Chronicles 20"; value: "R120_0";}
        ListElement { text: "1 Chronicles 21"; value: "R121_0";}
        ListElement { text: "1 Chronicles 22"; value: "R122_0";}
        ListElement { text: "1 Chronicles 23"; value: "R123_0";}
        ListElement { text: "1 Chronicles 24"; value: "R124_0";}
        ListElement { text: "1 Chronicles 25"; value: "R125_0";}
        ListElement { text: "1 Chronicles 26"; value: "R126_0";}
        ListElement { text: "1 Chronicles 27"; value: "R127_0";}
        ListElement { text: "1 Chronicles 28"; value: "R128_0";}
        ListElement { text: "1 Chronicles 29"; value: "R129_0";}
    }
}
