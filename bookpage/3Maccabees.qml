/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebook3ma
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">3 Maccabees</font>")

        } else {
            qsTr("<font color=\"dimgrey\">3 Maccabees</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescroll3ma + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("3Maccabees.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("3Maccabees.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernav3ma = true
                    settings.bookloc3ma = "#" + model.get(index).value
                    settings.finalurl3ma = "qrc:files/bible/3MA.xhtml" + settings.bookloc3ma
                    webview.url = settings.finalurl3ma
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescroll3ma: 0
        property string bookloc3ma: ""
        property string finalurl3ma: ""
        property bool isusernav3ma: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/3MA.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll3ma = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernav3ma == false) {
                    runJavaScript(biblebook3ma.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernav3ma = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "3 Maccabees 1"; value: "M31_0";}
        ListElement { text: "3 Maccabees 2"; value: "M32_0";}
        ListElement { text: "3 Maccabees 3"; value: "M33_0";}
        ListElement { text: "3 Maccabees 4"; value: "M34_0";}
        ListElement { text: "3 Maccabees 5"; value: "M35_0";}
        ListElement { text: "3 Maccabees 6"; value: "M36_0";}
        ListElement { text: "3 Maccabees 7"; value: "M37_0";}
    }
}
