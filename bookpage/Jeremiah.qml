/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookjer
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Jeremiah</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Jeremiah</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrolljer + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Jeremiah.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Jeremiah.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernav = true
                    settings.booklocjer = "#" + model.get(index).value
                    settings.finalurljer = "qrc:files/bible/JER.xhtml" + settings.booklocjer
                    webview.url = settings.finalurljer
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrolljer: 0
        property string booklocjer: ""
        property string finalurljer: ""
        property bool isusernav: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/JER.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolljer = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernav == false) {
                    runJavaScript(biblebookjer.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernav = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Jeremiah 1"; value: "JR1_0";}
        ListElement { text: "Jeremiah 2"; value: "JR2_0";}
        ListElement { text: "Jeremiah 3"; value: "JR3_0";}
        ListElement { text: "Jeremiah 4"; value: "JR4_0";}
        ListElement { text: "Jeremiah 5"; value: "JR5_0";}
        ListElement { text: "Jeremiah 6"; value: "JR6_0";}
        ListElement { text: "Jeremiah 7"; value: "JR7_0";}
        ListElement { text: "Jeremiah 8"; value: "JR8_0";}
        ListElement { text: "Jeremiah 9"; value: "JR9_0";}
        ListElement { text: "Jeremiah 10"; value: "JR10_0";}
        ListElement { text: "Jeremiah 11"; value: "JR11_0";}
        ListElement { text: "Jeremiah 12"; value: "JR12_0";}
        ListElement { text: "Jeremiah 13"; value: "JR13_0";}
        ListElement { text: "Jeremiah 14"; value: "JR14_0";}
        ListElement { text: "Jeremiah 15"; value: "JR15_0";}
        ListElement { text: "Jeremiah 16"; value: "JR16_0";}
        ListElement { text: "Jeremiah 17"; value: "JR17_0";}
        ListElement { text: "Jeremiah 18"; value: "JR18_0";}
        ListElement { text: "Jeremiah 19"; value: "JR19_0";}
        ListElement { text: "Jeremiah 20"; value: "JR20_0";}
        ListElement { text: "Jeremiah 21"; value: "JR21_0";}
        ListElement { text: "Jeremiah 22"; value: "JR22_0";}
        ListElement { text: "Jeremiah 23"; value: "JR23_0";}
        ListElement { text: "Jeremiah 24"; value: "JR24_0";}
        ListElement { text: "Jeremiah 25"; value: "JR25_0";}
        ListElement { text: "Jeremiah 26"; value: "JR26_0";}
        ListElement { text: "Jeremiah 27"; value: "JR27_0";}
        ListElement { text: "Jeremiah 28"; value: "JR28_0";}
        ListElement { text: "Jeremiah 29"; value: "JR29_0";}
        ListElement { text: "Jeremiah 30"; value: "JR30_0";}
        ListElement { text: "Jeremiah 31"; value: "JR31_0";}
        ListElement { text: "Jeremiah 32"; value: "JR32_0";}
        ListElement { text: "Jeremiah 33"; value: "JR33_0";}
        ListElement { text: "Jeremiah 34"; value: "JR34_0";}
        ListElement { text: "Jeremiah 35"; value: "JR35_0";}
        ListElement { text: "Jeremiah 36"; value: "JR36_0";}
        ListElement { text: "Jeremiah 37"; value: "JR37_0";}
        ListElement { text: "Jeremiah 38"; value: "JR38_0";}
        ListElement { text: "Jeremiah 39"; value: "JR39_0";}
        ListElement { text: "Jeremiah 40"; value: "JR40_0";}
        ListElement { text: "Jeremiah 41"; value: "JR41_0";}
        ListElement { text: "Jeremiah 42"; value: "JR42_0";}
        ListElement { text: "Jeremiah 43"; value: "JR43_0";}
        ListElement { text: "Jeremiah 44"; value: "JR44_0";}
        ListElement { text: "Jeremiah 45"; value: "JR45_0";}
        ListElement { text: "Jeremiah 46"; value: "JR46_0";}
        ListElement { text: "Jeremiah 47"; value: "JR47_0";}
        ListElement { text: "Jeremiah 48"; value: "JR48_0";}
        ListElement { text: "Jeremiah 49"; value: "JR49_0";}
        ListElement { text: "Jeremiah 50"; value: "JR50_0";}
        ListElement { text: "Jeremiah 51"; value: "JR51_0";}
        ListElement { text: "Jeremiah 52"; value: "JR52_0";}
    }
}
