/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookglo
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Glossary</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Glossary</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrollglo + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Glossary.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Glossary.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Letter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavglo = true
                    settings.booklocglo = "#" + model.get(index).value
                    settings.finalurlglo = "qrc:files/bible/GLO.xhtml" + settings.booklocglo
                    webview.url = settings.finalurlglo
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrollglo: 0
        property string booklocglo: ""
        property string finalurlglo: ""
        property bool isusernavglo: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/GLO.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollglo = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavglo == false) {
                    runJavaScript(biblebookglo.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavglo = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Glossary A"; value: "GL1_0";}
        ListElement { text: "Glossary B"; value: "GL2_0";}
        ListElement { text: "Glossary C"; value: "GL3_0";}
        ListElement { text: "Glossary D"; value: "GL4_0";}
        ListElement { text: "Glossary E"; value: "GL5_0";}
        ListElement { text: "Glossary G"; value: "GL7_0";}
        ListElement { text: "Glossary H"; value: "GL8_0";}
        ListElement { text: "Glossary I"; value: "GL9_0";}
        ListElement { text: "Glossary J"; value: "GL10_0";}
        ListElement { text: "Glossary K"; value: "GL11_0";}
        ListElement { text: "Glossary L"; value: "GL12_0";}
        ListElement { text: "Glossary M"; value: "GL13_0";}
        ListElement { text: "Glossary N"; value: "GL14_0";}
        ListElement { text: "Glossary O"; value: "GL15_0";}
        ListElement { text: "Glossary P"; value: "GL16_0";}
        ListElement { text: "Glossary Q"; value: "GL17_0";}
        ListElement { text: "Glossary R"; value: "GL18_0";}
        ListElement { text: "Glossary S"; value: "GL19_0";}
        ListElement { text: "Glossary T"; value: "GL20_0";}
        ListElement { text: "Glossary Y"; value: "GL25_0";}
        ListElement { text: "Glossary Z"; value: "GL26_0";}
    }
}
