/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebooksir
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Sirach</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Sirach</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrollsir + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Sirach.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Sirach.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavsir = true
                    settings.booklocsir = "#" + model.get(index).value
                    settings.finalurlsir = "qrc:files/bible/SIR.xhtml" + settings.booklocsir
                    webview.url = settings.finalurlsir
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrollsir: 0
        property string booklocsir: ""
        property string finalurlsir: ""
        property bool isusernavsir: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/SIR.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollsir = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavsir == false) {
                    runJavaScript(biblebooksir.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavsir = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Sirach 1"; value: "SR1_0";}
        ListElement { text: "Sirach 2"; value: "SR2_0";}
        ListElement { text: "Sirach 3"; value: "SR3_0";}
        ListElement { text: "Sirach 4"; value: "SR4_0";}
        ListElement { text: "Sirach 5"; value: "SR5_0";}
        ListElement { text: "Sirach 6"; value: "SR6_0";}
        ListElement { text: "Sirach 7"; value: "SR7_0";}
        ListElement { text: "Sirach 8"; value: "SR8_0";}
        ListElement { text: "Sirach 9"; value: "SR9_0";}
        ListElement { text: "Sirach 10"; value: "SR10_0";}
        ListElement { text: "Sirach 11"; value: "SR11_0";}
        ListElement { text: "Sirach 12"; value: "SR12_0";}
        ListElement { text: "Sirach 13"; value: "SR13_0";}
        ListElement { text: "Sirach 14"; value: "SR14_0";}
        ListElement { text: "Sirach 15"; value: "SR15_0";}
        ListElement { text: "Sirach 16"; value: "SR16_0";}
        ListElement { text: "Sirach 17"; value: "SR17_0";}
        ListElement { text: "Sirach 18"; value: "SR18_0";}
        ListElement { text: "Sirach 19"; value: "SR19_0";}
        ListElement { text: "Sirach 20"; value: "SR20_0";}
        ListElement { text: "Sirach 21"; value: "SR21_0";}
        ListElement { text: "Sirach 22"; value: "SR22_0";}
        ListElement { text: "Sirach 23"; value: "SR23_0";}
        ListElement { text: "Sirach 24"; value: "SR24_0";}
        ListElement { text: "Sirach 25"; value: "SR25_0";}
        ListElement { text: "Sirach 26"; value: "SR26_0";}
        ListElement { text: "Sirach 27"; value: "SR27_0";}
        ListElement { text: "Sirach 28"; value: "SR28_0";}
        ListElement { text: "Sirach 29"; value: "SR29_0";}
        ListElement { text: "Sirach 30"; value: "SR30_0";}
        ListElement { text: "Sirach 31"; value: "SR31_0";}
        ListElement { text: "Sirach 32"; value: "SR32_0";}
        ListElement { text: "Sirach 33"; value: "SR33_0";}
        ListElement { text: "Sirach 34"; value: "SR34_0";}
        ListElement { text: "Sirach 35"; value: "SR35_0";}
        ListElement { text: "Sirach 36"; value: "SR36_0";}
        ListElement { text: "Sirach 37"; value: "SR37_0";}
        ListElement { text: "Sirach 38"; value: "SR38_0";}
        ListElement { text: "Sirach 39"; value: "SR39_0";}
        ListElement { text: "Sirach 40"; value: "SR40_0";}
        ListElement { text: "Sirach 41"; value: "SR41_0";}
        ListElement { text: "Sirach 42"; value: "SR42_0";}
        ListElement { text: "Sirach 43"; value: "SR43_0";}
        ListElement { text: "Sirach 44"; value: "SR44_0";}
        ListElement { text: "Sirach 45"; value: "SR45_0";}
        ListElement { text: "Sirach 46"; value: "SR46_0";}
        ListElement { text: "Sirach 47"; value: "SR47_0";}
        ListElement { text: "Sirach 48"; value: "SR48_0";}
        ListElement { text: "Sirach 49"; value: "SR49_0";}
        ListElement { text: "Sirach 50"; value: "SR50_0";}
        ListElement { text: "Sirach 51"; value: "SR51_0";}
    }
}
