/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookpsa
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Psalms</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Psalms</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrollpsa + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Psalms.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Psalms.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavpsa = true
                    settings.booklocpsa = "#" + model.get(index).value
                    settings.finalurlpsa = "qrc:files/bible/PSA.xhtml" + settings.booklocpsa
                    webview.url = settings.finalurlpsa
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrollpsa: 0
        property string booklocpsa: ""
        property string finalurlpsa: ""
        property bool isusernavpsa: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/PSA.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollpsa = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavpsa == false) {
                    runJavaScript(biblebookpsa.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavpsa = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Psalms 1"; value: "PS1_0";}
        ListElement { text: "Psalms 2"; value: "PS2_0";}
        ListElement { text: "Psalms 3"; value: "PS3_0";}
        ListElement { text: "Psalms 4"; value: "PS4_0";}
        ListElement { text: "Psalms 5"; value: "PS5_0";}
        ListElement { text: "Psalms 6"; value: "PS6_0";}
        ListElement { text: "Psalms 7"; value: "PS7_0";}
        ListElement { text: "Psalms 8"; value: "PS8_0";}
        ListElement { text: "Psalms 9"; value: "PS9_0";}
        ListElement { text: "Psalms 10"; value: "PS10_0";}
        ListElement { text: "Psalms 11"; value: "PS11_0";}
        ListElement { text: "Psalms 12"; value: "PS12_0";}
        ListElement { text: "Psalms 13"; value: "PS13_0";}
        ListElement { text: "Psalms 14"; value: "PS14_0";}
        ListElement { text: "Psalms 15"; value: "PS15_0";}
        ListElement { text: "Psalms 16"; value: "PS16_0";}
        ListElement { text: "Psalms 17"; value: "PS17_0";}
        ListElement { text: "Psalms 18"; value: "PS18_0";}
        ListElement { text: "Psalms 19"; value: "PS19_0";}
        ListElement { text: "Psalms 20"; value: "PS20_0";}
        ListElement { text: "Psalms 21"; value: "PS21_0";}
        ListElement { text: "Psalms 22"; value: "PS22_0";}
        ListElement { text: "Psalms 23"; value: "PS23_0";}
        ListElement { text: "Psalms 24"; value: "PS24_0";}
        ListElement { text: "Psalms 25"; value: "PS25_0";}
        ListElement { text: "Psalms 26"; value: "PS26_0";}
        ListElement { text: "Psalms 27"; value: "PS27_0";}
        ListElement { text: "Psalms 28"; value: "PS28_0";}
        ListElement { text: "Psalms 29"; value: "PS29_0";}
        ListElement { text: "Psalms 30"; value: "PS30_0";}
        ListElement { text: "Psalms 31"; value: "PS31_0";}
        ListElement { text: "Psalms 32"; value: "PS32_0";}
        ListElement { text: "Psalms 33"; value: "PS33_0";}
        ListElement { text: "Psalms 34"; value: "PS34_0";}
        ListElement { text: "Psalms 35"; value: "PS35_0";}
        ListElement { text: "Psalms 36"; value: "PS36_0";}
        ListElement { text: "Psalms 37"; value: "PS37_0";}
        ListElement { text: "Psalms 38"; value: "PS38_0";}
        ListElement { text: "Psalms 39"; value: "PS39_0";}
        ListElement { text: "Psalms 40"; value: "PS40_0";}
        ListElement { text: "Psalms 41"; value: "PS41_0";}
        ListElement { text: "Psalms 42"; value: "PS42_0";}
        ListElement { text: "Psalms 43"; value: "PS43_0";}
        ListElement { text: "Psalms 44"; value: "PS44_0";}
        ListElement { text: "Psalms 45"; value: "PS45_0";}
        ListElement { text: "Psalms 46"; value: "PS46_0";}
        ListElement { text: "Psalms 47"; value: "PS47_0";}
        ListElement { text: "Psalms 48"; value: "PS48_0";}
        ListElement { text: "Psalms 49"; value: "PS49_0";}
        ListElement { text: "Psalms 50"; value: "PS50_0";}
        ListElement { text: "Psalms 51"; value: "PS51_0";}
        ListElement { text: "Psalms 52"; value: "PS52_0";}
        ListElement { text: "Psalms 53"; value: "PS53_0";}
        ListElement { text: "Psalms 54"; value: "PS54_0";}
        ListElement { text: "Psalms 55"; value: "PS55_0";}
        ListElement { text: "Psalms 56"; value: "PS56_0";}
        ListElement { text: "Psalms 57"; value: "PS57_0";}
        ListElement { text: "Psalms 58"; value: "PS58_0";}
        ListElement { text: "Psalms 59"; value: "PS59_0";}
        ListElement { text: "Psalms 60"; value: "PS60_0";}
        ListElement { text: "Psalms 61"; value: "PS61_0";}
        ListElement { text: "Psalms 62"; value: "PS62_0";}
        ListElement { text: "Psalms 63"; value: "PS63_0";}
        ListElement { text: "Psalms 64"; value: "PS64_0";}
        ListElement { text: "Psalms 65"; value: "PS65_0";}
        ListElement { text: "Psalms 66"; value: "PS66_0";}
        ListElement { text: "Psalms 67"; value: "PS67_0";}
        ListElement { text: "Psalms 68"; value: "PS68_0";}
        ListElement { text: "Psalms 69"; value: "PS69_0";}
        ListElement { text: "Psalms 70"; value: "PS70_0";}
        ListElement { text: "Psalms 71"; value: "PS71_0";}
        ListElement { text: "Psalms 72"; value: "PS72_0";}
        ListElement { text: "Psalms 73"; value: "PS73_0";}
        ListElement { text: "Psalms 74"; value: "PS74_0";}
        ListElement { text: "Psalms 75"; value: "PS75_0";}
        ListElement { text: "Psalms 76"; value: "PS76_0";}
        ListElement { text: "Psalms 77"; value: "PS77_0";}
        ListElement { text: "Psalms 78"; value: "PS78_0";}
        ListElement { text: "Psalms 79"; value: "PS79_0";}
        ListElement { text: "Psalms 80"; value: "PS80_0";}
        ListElement { text: "Psalms 81"; value: "PS81_0";}
        ListElement { text: "Psalms 82"; value: "PS82_0";}
        ListElement { text: "Psalms 83"; value: "PS83_0";}
        ListElement { text: "Psalms 84"; value: "PS84_0";}
        ListElement { text: "Psalms 85"; value: "PS85_0";}
        ListElement { text: "Psalms 86"; value: "PS86_0";}
        ListElement { text: "Psalms 87"; value: "PS87_0";}
        ListElement { text: "Psalms 88"; value: "PS88_0";}
        ListElement { text: "Psalms 89"; value: "PS89_0";}
        ListElement { text: "Psalms 90"; value: "PS90_0";}
        ListElement { text: "Psalms 91"; value: "PS91_0";}
        ListElement { text: "Psalms 92"; value: "PS92_0";}
        ListElement { text: "Psalms 93"; value: "PS93_0";}
        ListElement { text: "Psalms 94"; value: "PS94_0";}
        ListElement { text: "Psalms 95"; value: "PS95_0";}
        ListElement { text: "Psalms 96"; value: "PS96_0";}
        ListElement { text: "Psalms 97"; value: "PS97_0";}
        ListElement { text: "Psalms 98"; value: "PS98_0";}
        ListElement { text: "Psalms 99"; value: "PS99_0";}
        ListElement { text: "Psalms 100"; value: "PS100_0";}
        ListElement { text: "Psalms 101"; value: "PS101_0";}
        ListElement { text: "Psalms 102"; value: "PS102_0";}
        ListElement { text: "Psalms 103"; value: "PS103_0";}
        ListElement { text: "Psalms 104"; value: "PS104_0";}
        ListElement { text: "Psalms 105"; value: "PS105_0";}
        ListElement { text: "Psalms 106"; value: "PS106_0";}
        ListElement { text: "Psalms 107"; value: "PS107_0";}
        ListElement { text: "Psalms 108"; value: "PS108_0";}
        ListElement { text: "Psalms 109"; value: "PS109_0";}
        ListElement { text: "Psalms 110"; value: "PS110_0";}
        ListElement { text: "Psalms 111"; value: "PS111_0";}
        ListElement { text: "Psalms 112"; value: "PS112_0";}
        ListElement { text: "Psalms 113"; value: "PS113_0";}
        ListElement { text: "Psalms 114"; value: "PS114_0";}
        ListElement { text: "Psalms 115"; value: "PS115_0";}
        ListElement { text: "Psalms 116"; value: "PS116_0";}
        ListElement { text: "Psalms 117"; value: "PS117_0";}
        ListElement { text: "Psalms 118"; value: "PS118_0";}
        ListElement { text: "Psalms 119"; value: "PS119_0";}
        ListElement { text: "Psalms 120"; value: "PS120_0";}
        ListElement { text: "Psalms 121"; value: "PS121_0";}
        ListElement { text: "Psalms 122"; value: "PS122_0";}
        ListElement { text: "Psalms 123"; value: "PS123_0";}
        ListElement { text: "Psalms 124"; value: "PS124_0";}
        ListElement { text: "Psalms 125"; value: "PS125_0";}
        ListElement { text: "Psalms 126"; value: "PS126_0";}
        ListElement { text: "Psalms 127"; value: "PS127_0";}
        ListElement { text: "Psalms 128"; value: "PS128_0";}
        ListElement { text: "Psalms 129"; value: "PS129_0";}
        ListElement { text: "Psalms 130"; value: "PS130_0";}
        ListElement { text: "Psalms 131"; value: "PS131_0";}
        ListElement { text: "Psalms 132"; value: "PS132_0";}
        ListElement { text: "Psalms 133"; value: "PS133_0";}
        ListElement { text: "Psalms 134"; value: "PS134_0";}
        ListElement { text: "Psalms 135"; value: "PS135_0";}
        ListElement { text: "Psalms 136"; value: "PS136_0";}
        ListElement { text: "Psalms 137"; value: "PS137_0";}
        ListElement { text: "Psalms 138"; value: "PS138_0";}
        ListElement { text: "Psalms 139"; value: "PS139_0";}
        ListElement { text: "Psalms 140"; value: "PS140_0";}
        ListElement { text: "Psalms 141"; value: "PS141_0";}
        ListElement { text: "Psalms 142"; value: "PS142_0";}
        ListElement { text: "Psalms 143"; value: "PS143_0";}
        ListElement { text: "Psalms 144"; value: "PS144_0";}
        ListElement { text: "Psalms 145"; value: "PS145_0";}
        ListElement { text: "Psalms 146"; value: "PS146_0";}
        ListElement { text: "Psalms 147"; value: "PS147_0";}
        ListElement { text: "Psalms 148"; value: "PS148_0";}
        ListElement { text: "Psalms 149"; value: "PS149_0";}
        ListElement { text: "Psalms 150"; value: "PS150_0";}
    }
}
