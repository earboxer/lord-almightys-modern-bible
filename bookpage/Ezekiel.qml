/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookezk
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Ezekiel</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Ezekiel</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrollezk + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Ezekiel.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Ezekiel.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavezk = true
                    settings.booklocezk = "#" + model.get(index).value
                    settings.finalurlezk = "qrc:files/bible/EZK.xhtml" + settings.booklocezk
                    webview.url = settings.finalurlezk
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrollezk: 0
        property string booklocezk: ""
        property string finalurlezk: ""
        property bool isusernavezk: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/EZK.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollezk = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavezk == false) {
                    runJavaScript(biblebookezk.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavezk = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Ezekiel 1"; value: "EK1_0";}
        ListElement { text: "Ezekiel 2"; value: "EK2_0";}
        ListElement { text: "Ezekiel 3"; value: "EK3_0";}
        ListElement { text: "Ezekiel 4"; value: "EK4_0";}
        ListElement { text: "Ezekiel 5"; value: "EK5_0";}
        ListElement { text: "Ezekiel 6"; value: "EK6_0";}
        ListElement { text: "Ezekiel 7"; value: "EK7_0";}
        ListElement { text: "Ezekiel 8"; value: "EK8_0";}
        ListElement { text: "Ezekiel 9"; value: "EK9_0";}
        ListElement { text: "Ezekiel 10"; value: "EK10_0";}
        ListElement { text: "Ezekiel 11"; value: "EK11_0";}
        ListElement { text: "Ezekiel 12"; value: "EK12_0";}
        ListElement { text: "Ezekiel 13"; value: "EK13_0";}
        ListElement { text: "Ezekiel 14"; value: "EK14_0";}
        ListElement { text: "Ezekiel 15"; value: "EK15_0";}
        ListElement { text: "Ezekiel 16"; value: "EK16_0";}
        ListElement { text: "Ezekiel 17"; value: "EK17_0";}
        ListElement { text: "Ezekiel 18"; value: "EK18_0";}
        ListElement { text: "Ezekiel 19"; value: "EK19_0";}
        ListElement { text: "Ezekiel 20"; value: "EK20_0";}
        ListElement { text: "Ezekiel 21"; value: "EK21_0";}
        ListElement { text: "Ezekiel 22"; value: "EK22_0";}
        ListElement { text: "Ezekiel 23"; value: "EK23_0";}
        ListElement { text: "Ezekiel 24"; value: "EK24_0";}
        ListElement { text: "Ezekiel 25"; value: "EK25_0";}
        ListElement { text: "Ezekiel 26"; value: "EK26_0";}
        ListElement { text: "Ezekiel 27"; value: "EK27_0";}
        ListElement { text: "Ezekiel 28"; value: "EK28_0";}
        ListElement { text: "Ezekiel 29"; value: "EK29_0";}
        ListElement { text: "Ezekiel 30"; value: "EK30_0";}
        ListElement { text: "Ezekiel 31"; value: "EK31_0";}
        ListElement { text: "Ezekiel 32"; value: "EK32_0";}
        ListElement { text: "Ezekiel 33"; value: "EK33_0";}
        ListElement { text: "Ezekiel 34"; value: "EK34_0";}
        ListElement { text: "Ezekiel 35"; value: "EK35_0";}
        ListElement { text: "Ezekiel 36"; value: "EK36_0";}
        ListElement { text: "Ezekiel 37"; value: "EK37_0";}
        ListElement { text: "Ezekiel 38"; value: "EK38_0";}
        ListElement { text: "Ezekiel 39"; value: "EK39_0";}
        ListElement { text: "Ezekiel 40"; value: "EK40_0";}
        ListElement { text: "Ezekiel 41"; value: "EK41_0";}
        ListElement { text: "Ezekiel 42"; value: "EK42_0";}
        ListElement { text: "Ezekiel 43"; value: "EK43_0";}
        ListElement { text: "Ezekiel 44"; value: "EK44_0";}
        ListElement { text: "Ezekiel 45"; value: "EK45_0";}
        ListElement { text: "Ezekiel 46"; value: "EK46_0";}
        ListElement { text: "Ezekiel 47"; value: "EK47_0";}
        ListElement { text: "Ezekiel 48"; value: "EK48_0";}
    }
}
