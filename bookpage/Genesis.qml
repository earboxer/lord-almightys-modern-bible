/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookgen
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Genesis</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Genesis</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescroll + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Genesis.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Genesis.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernav = true
                    settings.bookloc = "#" + model.get(index).value
                    settings.finalurl = "qrc:files/bible/GEN.xhtml" + settings.bookloc
                    webview.url = settings.finalurl
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescroll: 0
        property string bookloc: ""
        property string finalurl: ""
        property bool isusernav: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/GEN.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescroll = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernav == false) {
                    runJavaScript(biblebookgen.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernav = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Genesis 1"; value: "GN1_0";}
        ListElement { text: "Genesis 2"; value: "GN2_0";}
        ListElement { text: "Genesis 3"; value: "GN3_0";}
        ListElement { text: "Genesis 4"; value: "GN4_0";}
        ListElement { text: "Genesis 5"; value: "GN5_0";}
        ListElement { text: "Genesis 6"; value: "GN6_0";}
        ListElement { text: "Genesis 7"; value: "GN7_0";}
        ListElement { text: "Genesis 8"; value: "GN8_0";}
        ListElement { text: "Genesis 9"; value: "GN9_0";}
        ListElement { text: "Genesis 10"; value: "GN10_0";}
        ListElement { text: "Genesis 11"; value: "GN11_0";}
        ListElement { text: "Genesis 12"; value: "GN12_0";}
        ListElement { text: "Genesis 13"; value: "GN13_0";}
        ListElement { text: "Genesis 14"; value: "GN14_0";}
        ListElement { text: "Genesis 15"; value: "GN15_0";}
        ListElement { text: "Genesis 16"; value: "GN16_0";}
        ListElement { text: "Genesis 17"; value: "GN17_0";}
        ListElement { text: "Genesis 18"; value: "GN18_0";}
        ListElement { text: "Genesis 19"; value: "GN19_0";}
        ListElement { text: "Genesis 20"; value: "GN20_0";}
        ListElement { text: "Genesis 21"; value: "GN21_0";}
        ListElement { text: "Genesis 22"; value: "GN22_0";}
        ListElement { text: "Genesis 23"; value: "GN23_0";}
        ListElement { text: "Genesis 24"; value: "GN24_0";}
        ListElement { text: "Genesis 25"; value: "GN25_0";}
        ListElement { text: "Genesis 26"; value: "GN26_0";}
        ListElement { text: "Genesis 27"; value: "GN27_0";}
        ListElement { text: "Genesis 28"; value: "GN28_0";}
        ListElement { text: "Genesis 29"; value: "GN29_0";}
        ListElement { text: "Genesis 30"; value: "GN30_0";}
        ListElement { text: "Genesis 31"; value: "GN31_0";}
        ListElement { text: "Genesis 32"; value: "GN32_0";}
        ListElement { text: "Genesis 33"; value: "GN33_0";}
        ListElement { text: "Genesis 34"; value: "GN34_0";}
        ListElement { text: "Genesis 35"; value: "GN35_0";}
        ListElement { text: "Genesis 36"; value: "GN36_0";}
        ListElement { text: "Genesis 37"; value: "GN37_0";}
        ListElement { text: "Genesis 38"; value: "GN38_0";}
        ListElement { text: "Genesis 39"; value: "GN39_0";}
        ListElement { text: "Genesis 40"; value: "GN40_0";}
        ListElement { text: "Genesis 41"; value: "GN41_0";}
        ListElement { text: "Genesis 42"; value: "GN42_0";}
        ListElement { text: "Genesis 43"; value: "GN43_0";}
        ListElement { text: "Genesis 44"; value: "GN44_0";}
        ListElement { text: "Genesis 45"; value: "GN45_0";}
        ListElement { text: "Genesis 46"; value: "GN46_0";}
        ListElement { text: "Genesis 47"; value: "GN47_0";}
        ListElement { text: "Genesis 48"; value: "GN48_0";}
        ListElement { text: "Genesis 49"; value: "GN49_0";}
        ListElement { text: "Genesis 50"; value: "GN50_0";}
    }
}
