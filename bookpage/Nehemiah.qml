/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookneh
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Nehemiah</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Nehemiah</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrollneh + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Nehemiah.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Nehemiah.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavneh = true
                    settings.booklocneh = "#" + model.get(index).value
                    settings.finalurlneh = "qrc:files/bible/NEH.xhtml" + settings.booklocneh
                    webview.url = settings.finalurlneh
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrollneh: 0
        property string booklocneh: ""
        property string finalurlneh: ""
        property bool isusernavneh: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/NEH.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollneh = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavneh == false) {
                    runJavaScript(biblebookneh.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavneh = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Nehemiah 1"; value: "NH1_0";}
        ListElement { text: "Nehemiah 2"; value: "NH2_0";}
        ListElement { text: "Nehemiah 3"; value: "NH3_0";}
        ListElement { text: "Nehemiah 4"; value: "NH4_0";}
        ListElement { text: "Nehemiah 5"; value: "NH5_0";}
        ListElement { text: "Nehemiah 6"; value: "NH6_0";}
        ListElement { text: "Nehemiah 7"; value: "NH7_0";}
        ListElement { text: "Nehemiah 8"; value: "NH8_0";}
        ListElement { text: "Nehemiah 9"; value: "NH9_0";}
        ListElement { text: "Nehemiah 10"; value: "NH10_0";}
        ListElement { text: "Nehemiah 11"; value: "NH11_0";}
        ListElement { text: "Nehemiah 12"; value: "NH12_0";}
        ListElement { text: "Nehemiah 13"; value: "NH13_0";}
    }
}
