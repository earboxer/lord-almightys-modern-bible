/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookwis
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Wisdom of Solomon</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Wisdom of Solomon</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrollwis + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Wisdom-of-Solomon.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Wisdom-of-Solomon.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 210
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavwis = true
                    settings.booklocwis = "#" + model.get(index).value
                    settings.finalurlwis = "qrc:files/bible/WIS.xhtml" + settings.booklocwis
                    webview.url = settings.finalurlwis
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrollwis: 0
        property string booklocwis: ""
        property string finalurlwis: ""
        property bool isusernavwis: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/WIS.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollwis = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavwis == false) {
                    runJavaScript(biblebookwis.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavwis = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Wisdom of Solomon 1"; value: "WS1_0";}
        ListElement { text: "Wisdom of Solomon 2"; value: "WS2_0";}
        ListElement { text: "Wisdom of Solomon 3"; value: "WS3_0";}
        ListElement { text: "Wisdom of Solomon 4"; value: "WS4_0";}
        ListElement { text: "Wisdom of Solomon 5"; value: "WS5_0";}
        ListElement { text: "Wisdom of Solomon 6"; value: "WS6_0";}
        ListElement { text: "Wisdom of Solomon 7"; value: "WS7_0";}
        ListElement { text: "Wisdom of Solomon 8"; value: "WS8_0";}
        ListElement { text: "Wisdom of Solomon 9"; value: "WS9_0";}
        ListElement { text: "Wisdom of Solomon 10"; value: "WS10_0";}
        ListElement { text: "Wisdom of Solomon 11"; value: "WS11_0";}
        ListElement { text: "Wisdom of Solomon 12"; value: "WS12_0";}
        ListElement { text: "Wisdom of Solomon 13"; value: "WS13_0";}
        ListElement { text: "Wisdom of Solomon 14"; value: "WS14_0";}
        ListElement { text: "Wisdom of Solomon 15"; value: "WS15_0";}
        ListElement { text: "Wisdom of Solomon 16"; value: "WS16_0";}
        ListElement { text: "Wisdom of Solomon 17"; value: "WS17_0";}
        ListElement { text: "Wisdom of Solomon 18"; value: "WS18_0";}
        ListElement { text: "Wisdom of Solomon 19"; value: "WS19_0";}
    }
}
