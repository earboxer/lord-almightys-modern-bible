/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookdeu
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Deuteronomy</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Deuteronomy</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrolldeu + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Deuteronomy.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Deuteronomy.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavdeu = true
                    settings.booklocdeu = "#" + model.get(index).value
                    settings.finalurlexo = "qrc:files/bible/DEU.xhtml" + settings.booklocdeu
                    webview.url = settings.finalurlexo
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrolldeu: 0
        property string booklocdeu: ""
        property string finalurlexo: ""
        property bool isusernavdeu: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/DEU.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolldeu = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavdeu == false) {
                    runJavaScript(biblebookdeu.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavdeu = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Deuteronomy 1"; value: "DT1_0";}
        ListElement { text: "Deuteronomy 2"; value: "DT2_0";}
        ListElement { text: "Deuteronomy 3"; value: "DT3_0";}
        ListElement { text: "Deuteronomy 4"; value: "DT4_0";}
        ListElement { text: "Deuteronomy 5"; value: "DT5_0";}
        ListElement { text: "Deuteronomy 6"; value: "DT6_0";}
        ListElement { text: "Deuteronomy 7"; value: "DT7_0";}
        ListElement { text: "Deuteronomy 8"; value: "DT8_0";}
        ListElement { text: "Deuteronomy 9"; value: "DT9_0";}
        ListElement { text: "Deuteronomy 10"; value: "DT10_0";}
        ListElement { text: "Deuteronomy 11"; value: "DT11_0";}
        ListElement { text: "Deuteronomy 12"; value: "DT12_0";}
        ListElement { text: "Deuteronomy 13"; value: "DT13_0";}
        ListElement { text: "Deuteronomy 14"; value: "DT14_0";}
        ListElement { text: "Deuteronomy 15"; value: "DT15_0";}
        ListElement { text: "Deuteronomy 16"; value: "DT16_0";}
        ListElement { text: "Deuteronomy 17"; value: "DT17_0";}
        ListElement { text: "Deuteronomy 18"; value: "DT18_0";}
        ListElement { text: "Deuteronomy 19"; value: "DT19_0";}
        ListElement { text: "Deuteronomy 20"; value: "DT20_0";}
        ListElement { text: "Deuteronomy 21"; value: "DT21_0";}
        ListElement { text: "Deuteronomy 22"; value: "DT22_0";}
        ListElement { text: "Deuteronomy 23"; value: "DT23_0";}
        ListElement { text: "Deuteronomy 24"; value: "DT24_0";}
        ListElement { text: "Deuteronomy 25"; value: "DT25_0";}
        ListElement { text: "Deuteronomy 26"; value: "DT26_0";}
        ListElement { text: "Deuteronomy 27"; value: "DT27_0";}
        ListElement { text: "Deuteronomy 28"; value: "DT28_0";}
        ListElement { text: "Deuteronomy 29"; value: "DT29_0";}
        ListElement { text: "Deuteronomy 30"; value: "DT30_0";}
        ListElement { text: "Deuteronomy 31"; value: "DT31_0";}
        ListElement { text: "Deuteronomy 32"; value: "DT32_0";}
        ListElement { text: "Deuteronomy 33"; value: "DT33_0";}
        ListElement { text: "Deuteronomy 34"; value: "DT34_0";}
    }
}
