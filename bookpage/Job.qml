/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookjob
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Job</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Job</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrolljob + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Job.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Job.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavjob = true
                    settings.booklocjob = "#" + model.get(index).value
                    settings.finalurljob = "qrc:files/bible/JOB.xhtml" + settings.booklocjob
                    webview.url = settings.finalurljob
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrolljob: 0
        property string booklocjob: ""
        property string finalurljob: ""
        property bool isusernavjob: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/JOB.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolljob = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavjob == false) {
                    runJavaScript(biblebookjob.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavjob = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Job 1"; value: "JB1_0";}
        ListElement { text: "Job 2"; value: "JB2_0";}
        ListElement { text: "Job 3"; value: "JB3_0";}
        ListElement { text: "Job 4"; value: "JB4_0";}
        ListElement { text: "Job 5"; value: "JB5_0";}
        ListElement { text: "Job 6"; value: "JB6_0";}
        ListElement { text: "Job 7"; value: "JB7_0";}
        ListElement { text: "Job 8"; value: "JB8_0";}
        ListElement { text: "Job 9"; value: "JB9_0";}
        ListElement { text: "Job 10"; value: "JB10_0";}
        ListElement { text: "Job 11"; value: "JB11_0";}
        ListElement { text: "Job 12"; value: "JB12_0";}
        ListElement { text: "Job 13"; value: "JB13_0";}
        ListElement { text: "Job 14"; value: "JB14_0";}
        ListElement { text: "Job 15"; value: "JB15_0";}
        ListElement { text: "Job 16"; value: "JB16_0";}
        ListElement { text: "Job 17"; value: "JB17_0";}
        ListElement { text: "Job 18"; value: "JB18_0";}
        ListElement { text: "Job 19"; value: "JB19_0";}
        ListElement { text: "Job 20"; value: "JB20_0";}
        ListElement { text: "Job 21"; value: "JB21_0";}
        ListElement { text: "Job 22"; value: "JB22_0";}
        ListElement { text: "Job 23"; value: "JB23_0";}
        ListElement { text: "Job 24"; value: "JB24_0";}
        ListElement { text: "Job 25"; value: "JB25_0";}
        ListElement { text: "Job 26"; value: "JB26_0";}
        ListElement { text: "Job 27"; value: "JB27_0";}
        ListElement { text: "Job 28"; value: "JB28_0";}
        ListElement { text: "Job 29"; value: "JB29_0";}
        ListElement { text: "Job 30"; value: "JB30_0";}
        ListElement { text: "Job 31"; value: "JB31_0";}
        ListElement { text: "Job 32"; value: "JB32_0";}
        ListElement { text: "Job 33"; value: "JB33_0";}
        ListElement { text: "Job 34"; value: "JB34_0";}
        ListElement { text: "Job 35"; value: "JB35_0";}
        ListElement { text: "Job 36"; value: "JB36_0";}
        ListElement { text: "Job 37"; value: "JB37_0";}
        ListElement { text: "Job 38"; value: "JB38_0";}
        ListElement { text: "Job 39"; value: "JB39_0";}
        ListElement { text: "Job 40"; value: "JB40_0";}
        ListElement { text: "Job 41"; value: "JB41_0";}
        ListElement { text: "Job 42"; value: "JB42_0";}
    }
}
