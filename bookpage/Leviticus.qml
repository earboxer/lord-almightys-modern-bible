/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebooklev
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Leviticus</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Leviticus</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrolllev + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Leviticus.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Leviticus.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavlev = true
                    settings.bookloclev = "#" + model.get(index).value
                    settings.finalurllev = "qrc:files/bible/LEV.xhtml" + settings.bookloclev
                    webview.url = settings.finalurllev
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrolllev: 0
        property string bookloclev: ""
        property string finalurllev: ""
        property bool isusernavlev: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/LEV.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolllev = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavlev == false) {
                    runJavaScript(biblebooklev.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavlev = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Leviticus 1"; value: "LV1_0";}
        ListElement { text: "Leviticus 2"; value: "LV2_0";}
        ListElement { text: "Leviticus 3"; value: "LV3_0";}
        ListElement { text: "Leviticus 4"; value: "LV4_0";}
        ListElement { text: "Leviticus 5"; value: "LV5_0";}
        ListElement { text: "Leviticus 6"; value: "LV6_0";}
        ListElement { text: "Leviticus 7"; value: "LV7_0";}
        ListElement { text: "Leviticus 8"; value: "LV8_0";}
        ListElement { text: "Leviticus 9"; value: "LV9_0";}
        ListElement { text: "Leviticus 10"; value: "LV10_0";}
        ListElement { text: "Leviticus 11"; value: "LV11_0";}
        ListElement { text: "Leviticus 12"; value: "LV12_0";}
        ListElement { text: "Leviticus 13"; value: "LV13_0";}
        ListElement { text: "Leviticus 14"; value: "LV14_0";}
        ListElement { text: "Leviticus 15"; value: "LV15_0";}
        ListElement { text: "Leviticus 16"; value: "LV16_0";}
        ListElement { text: "Leviticus 17"; value: "LV17_0";}
        ListElement { text: "Leviticus 18"; value: "LV18_0";}
        ListElement { text: "Leviticus 19"; value: "LV19_0";}
        ListElement { text: "Leviticus 20"; value: "LV20_0";}
        ListElement { text: "Leviticus 21"; value: "LV21_0";}
        ListElement { text: "Leviticus 22"; value: "LV22_0";}
        ListElement { text: "Leviticus 23"; value: "LV23_0";}
        ListElement { text: "Leviticus 24"; value: "LV24_0";}
        ListElement { text: "Leviticus 25"; value: "LV25_0";}
        ListElement { text: "Leviticus 26"; value: "LV26_0";}
        ListElement { text: "Leviticus 27"; value: "LV27_0";}
    }
}
