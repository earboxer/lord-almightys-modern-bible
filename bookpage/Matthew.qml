/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookmat
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Matthew</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Matthew</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrollmat + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Matthew.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Matthew.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavmat = true
                    settings.booklocmat = "#" + model.get(index).value
                    settings.finalurlmat = "qrc:files/bible/MAT.xhtml" + settings.booklocmat
                    webview.url = settings.finalurlmat
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrollmat: 0
        property string booklocmat: ""
        property string finalurlmat: ""
        property bool isusernavmat: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/MAT.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollmat = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavmat == false) {
                    runJavaScript(biblebookmat.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavmat = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Matthew 1"; value: "MT1_0";}
        ListElement { text: "Matthew 2"; value: "MT2_0";}
        ListElement { text: "Matthew 3"; value: "MT3_0";}
        ListElement { text: "Matthew 4"; value: "MT4_0";}
        ListElement { text: "Matthew 5"; value: "MT5_0";}
        ListElement { text: "Matthew 6"; value: "MT6_0";}
        ListElement { text: "Matthew 7"; value: "MT7_0";}
        ListElement { text: "Matthew 8"; value: "MT8_0";}
        ListElement { text: "Matthew 9"; value: "MT9_0";}
        ListElement { text: "Matthew 10"; value: "MT10_0";}
        ListElement { text: "Matthew 11"; value: "MT11_0";}
        ListElement { text: "Matthew 12"; value: "MT12_0";}
        ListElement { text: "Matthew 13"; value: "MT13_0";}
        ListElement { text: "Matthew 14"; value: "MT14_0";}
        ListElement { text: "Matthew 15"; value: "MT15_0";}
        ListElement { text: "Matthew 16"; value: "MT16_0";}
        ListElement { text: "Matthew 17"; value: "MT17_0";}
        ListElement { text: "Matthew 18"; value: "MT18_0";}
        ListElement { text: "Matthew 19"; value: "MT19_0";}
        ListElement { text: "Matthew 20"; value: "MT20_0";}
        ListElement { text: "Matthew 21"; value: "MT21_0";}
        ListElement { text: "Matthew 22"; value: "MT22_0";}
        ListElement { text: "Matthew 23"; value: "MT23_0";}
        ListElement { text: "Matthew 24"; value: "MT24_0";}
        ListElement { text: "Matthew 25"; value: "MT25_0";}
        ListElement { text: "Matthew 26"; value: "MT26_0";}
        ListElement { text: "Matthew 27"; value: "MT27_0";}
        ListElement { text: "Matthew 28"; value: "MT28_0";}
    }
}
