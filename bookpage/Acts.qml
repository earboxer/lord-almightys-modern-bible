/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookact
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Acts</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Acts</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrollact + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Acts.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Acts.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavact = true
                    settings.booklocact = "#" + model.get(index).value
                    settings.finalurlact = "qrc:files/bible/ACT.xhtml" + settings.booklocact
                    webview.url = settings.finalurlact
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrollact: 0
        property string booklocact: ""
        property string finalurlact: ""
        property bool isusernavact: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/ACT.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrollact = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavact == false) {
                    runJavaScript(biblebookact.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavact = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Acts 1"; value: "AC1_0";}
        ListElement { text: "Acts 2"; value: "AC2_0";}
        ListElement { text: "Acts 3"; value: "AC3_0";}
        ListElement { text: "Acts 4"; value: "AC4_0";}
        ListElement { text: "Acts 5"; value: "AC5_0";}
        ListElement { text: "Acts 6"; value: "AC6_0";}
        ListElement { text: "Acts 7"; value: "AC7_0";}
        ListElement { text: "Acts 8"; value: "AC8_0";}
        ListElement { text: "Acts 9"; value: "AC9_0";}
        ListElement { text: "Acts 10"; value: "AC10_0";}
        ListElement { text: "Acts 11"; value: "AC11_0";}
        ListElement { text: "Acts 12"; value: "AC12_0";}
        ListElement { text: "Acts 13"; value: "AC13_0";}
        ListElement { text: "Acts 14"; value: "AC14_0";}
        ListElement { text: "Acts 15"; value: "AC15_0";}
        ListElement { text: "Acts 16"; value: "AC16_0";}
        ListElement { text: "Acts 17"; value: "AC17_0";}
        ListElement { text: "Acts 18"; value: "AC18_0";}
        ListElement { text: "Acts 19"; value: "AC19_0";}
        ListElement { text: "Acts 20"; value: "AC20_0";}
        ListElement { text: "Acts 21"; value: "AC21_0";}
        ListElement { text: "Acts 22"; value: "AC22_0";}
        ListElement { text: "Acts 23"; value: "AC23_0";}
        ListElement { text: "Acts 24"; value: "AC24_0";}
        ListElement { text: "Acts 25"; value: "AC25_0";}
        ListElement { text: "Acts 26"; value: "AC26_0";}
        ListElement { text: "Acts 27"; value: "AC27_0";}
        ListElement { text: "Acts 28"; value: "AC28_0";}
    }
}
