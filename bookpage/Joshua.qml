/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles 1.4
import QtWebEngine 1.8
import Qt.labs.settings 1.1

Page {
    id: biblebookjos
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">Joshua</font>")

        } else {
            qsTr("<font color=\"dimgrey\">Joshua</font>")
        }
    }
    property string pagepos: "window.scrollTo(0," + settings.restorescrolljos + ");"
    footer: ToolBar {
        contentHeight: toolButton.implicitHeight
        Material.theme: {
                    if (themeSwitch.checked) {
                        Material.primary="dimgrey"
                        Material.foreground="antiquewhite"
                        Material.background="dimgrey"
                        Material.accent="antiquewhite"
                        stackView.replace("Joshua.qml")
                    } else {
                        Material.primary="antiquewhite"
                        Material.foreground="dimgrey"
                        Material.background="antiquewhite"
                        Material.accent="dimgrey"
                        stackView.replace("Joshua.qml")
                    }
                }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: toolButtonZI
                text: "+ In"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel += 0.1
                    webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
                }
            }
            ToolButton {
                id: toolButtonZO
                text: "- Out"
                font.pixelSize: Qt.application.font.pixelSize
                onClicked: {
                    settings.zoomlevel -= 0.1
                    webview.runJavaScript("document.body.style.zoom= %1;".arg(settings.zoomlevel))
                }
            }
            ToolSeparator {
                padding: 0
                topPadding: 12
                bottomPadding: 12
                orientation: Qt.Vertical
                height: parent.height
                contentItem: Rectangle {
                    implicitWidth: 1
                    color: {
                        if(themeSwitch.checked){
                            "antiquewhite"
                        } else {
                            "dimgrey"
                        }
                    }
                }
            }
            ComboBox {
                id: control
                model: chaptermodel
                textRole: "text"
                displayText: "Select Chapter "
                width: 200
                contentItem: Text {
                    leftPadding: 10
                    rightPadding: control.indicator.width + control.spacing
                    text: control.displayText
                    font: Qt.application.font.pixelSize
                    color: if(themeSwitch.checked){
                               "antiquewhite"
                           } else {
                               "dimgrey"
                           }
                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                }
                popup: Popup {
                        y: control.height + 40
                        width: control.width
                        implicitHeight: contentItem.implicitHeight
                        padding: 1
                        contentItem: ListView {
                            clip: true
                            implicitHeight: 400
                            bottomMargin: 30
                            model: control.popup.visible ? control.delegateModel : null
                            currentIndex: control.highlightedIndex
                            ScrollIndicator.vertical: ScrollIndicator { }
                        }
                }
                indicator: Canvas {
                        id: canvas
                        x: control.width - width - control.rightPadding
                        y: control.topPadding + (control.availableHeight - height) / 2
                        width: 12
                        height: 8
                        contextType: "2d"
                        Connections {
                            target: control
                            onPressedChanged: canvas.requestPaint()
                        }
                        onPaint: {
                            if(themeSwitch.checked){
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "white" : "antiquewhite";
                                context.fill();
                            } else {
                                context.reset();
                                context.moveTo(0, 0);
                                context.lineTo(width, 0);
                                context.lineTo(width / 2, height);
                                context.closePath();
                                context.fillStyle = control.pressed ? "black" : "dimgrey";
                                context.fill();
                            }
                        }
                    }
                onActivated: {
                    settings.isusernavjos = true
                    settings.booklocjos = "#" + model.get(index).value
                    settings.finalurljos = "qrc:files/bible/JOS.xhtml" + settings.booklocjos
                    webview.url = settings.finalurljos
                }
            }
        }
    }
    Settings {
        id: settings
        property real zoomlevel: 1.0
        property real restorescrolljos: 0
        property string booklocjos: ""
        property string finalurljos: ""
        property bool isusernavjos: false
    }
    WebEngineView {
        id: webview
        anchors.fill: parent
        anchors.margins: 10
        backgroundColor: "#00000000"
        settings.showScrollBars: false
        settings.javascriptEnabled: true
        settings.localContentCanAccessFileUrls: true
        url: "qrc:files/bible/JOS.xhtml"
        onScrollPositionChanged: {
            runJavaScript("window.pageYOffset;", function(result) {settings.restorescrolljos = (result);})
        }
        onLoadProgressChanged: {
            loadpop.opacity = 1
            webview.runJavaScript("document.body.style.zoom= %1".arg(settings.zoomlevel))
            if (themeSwitch.checked) {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-dark.css);'; document.head.appendChild(loadcss);")
            } else {
                runJavaScript("var loadcss = document.createElement('style'); loadcss.innerHTML = '@import url(web-light.css);'; document.head.appendChild(loadcss);")
            }
            if (loadProgress == 100) {
                if (settings.isusernavjos == false) {
                    runJavaScript(biblebookjos.pagepos)
                }
                loadpop.opacity = 0
                settings.isusernavjos = false
            }
        }
    }
    Rectangle {
            id: loadpop
            width: parent.width
            height: parent.height
            color: {
                if(themeSwitch.checked){
                    "dimgrey"
                } else {
                    "antiquewhite"
                }
            }
            Column {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                Label {
                    id: loadingtext
                    text: "Loading Book..."
                    font.pixelSize: Qt.application.font.pixelSize * 3
                    horizontalAlignment: Text.AlignLeft
                    textFormat: Text.RichText
                }
            }
    }
    ListModel {
        id: chaptermodel
        ListElement { text: "Joshua 1"; value: "JS1_0";}
        ListElement { text: "Joshua 2"; value: "JS2_0";}
        ListElement { text: "Joshua 3"; value: "JS3_0";}
        ListElement { text: "Joshua 4"; value: "JS4_0";}
        ListElement { text: "Joshua 5"; value: "JS5_0";}
        ListElement { text: "Joshua 6"; value: "JS6_0";}
        ListElement { text: "Joshua 7"; value: "JS7_0";}
        ListElement { text: "Joshua 8"; value: "JS8_0";}
        ListElement { text: "Joshua 9"; value: "JS9_0";}
        ListElement { text: "Joshua 10"; value: "JS10_0";}
        ListElement { text: "Joshua 11"; value: "JS11_0";}
        ListElement { text: "Joshua 12"; value: "JS12_0";}
        ListElement { text: "Joshua 13"; value: "JS13_0";}
        ListElement { text: "Joshua 14"; value: "JS14_0";}
        ListElement { text: "Joshua 15"; value: "JS15_0";}
        ListElement { text: "Joshua 16"; value: "JS16_0";}
        ListElement { text: "Joshua 17"; value: "JS17_0";}
        ListElement { text: "Joshua 18"; value: "JS18_0";}
        ListElement { text: "Joshua 19"; value: "JS19_0";}
        ListElement { text: "Joshua 20"; value: "JS20_0";}
        ListElement { text: "Joshua 21"; value: "JS21_0";}
        ListElement { text: "Joshua 22"; value: "JS22_0";}
        ListElement { text: "Joshua 23"; value: "JS23_0";}
        ListElement { text: "Joshua 24"; value: "JS24_0";}
    }
}
