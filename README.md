# Description

Lord Almighty's Modern Bible (LAMB) is a cross platform Bible application. It was designed with Linux Mobile in mind and provides an offline copy of the Bible in an elegant look and feel. 

This app was developed to spread scripture to the security minded individuals that need or prefer to read the bible offline for various reasons. The hope is that this app will help to bring scripture to many people across the world who may not have access to stable internet, high-end devices or a safe place to read it online.

The Bible text and glossary in this app are based on the 2020 edition of the World English Bible. Neither the scripture nor the translations in Lord Almighty's Modern Bible have been modified from the World English Bible, however, there were major modifications made to the footnotes display and some text from the WEB version. Each footnote has been reviewed for context and placement. They have been put into a more modern and mobile compatable tooltip popup. The word/words/phrase that they apply to have also been underlined. Quotes from other sections of the Bible have also been moved into popups to prevent the reader from having to skip around. This I hope will help the reader gain a better understanding of what they are reading. 

The World English Bible with glossary is a Public Domain (no copyright) modern English translation of the Holy Bible, and can be found at https://worldenglish.bible

While the Bible text and glossary used in this app are in the public domain, this application and its code are copyrighted and released under a GPL v3 license.

# Install

**Manjaro/Arch Mobile (Phosh and Plasma) and Desktop**

**[Pre-built Binaries](https://gitlab.com/The3DmaN/the3dman.gitlab.io/-/tree/master/content/apps/LAMB)**


**Build from source:**

Option 1:

Install from AUR with Discover or from AUR with yay: `yay lord-almightys-modern-bible-git`

Option 2:

1) run `sudo pacman -S base-devel` (only if you don't have this installed already)

2) Download PKGBUILD file from above

2) cd into directory you download to

3) run `makepkg -si`

# Screenshots

![Screenshot](img/lightmode1.png "Light Mode Screenshot")

![Screenshot](img/lightmode2.png "Light Mode With Footnote Popup Screenshot")

![Screenshot](img/darkmode1.png "Dark Mode Screenshot")

# Current Books Included

All Old Testament Books (with all footnotes)

All Deuterocanon Books (most but not all footnotes finished)

All New Testament Books (with all footnotes)

# Known Issues

- Plasma Mobile ignores menu buttons (you can swipe to the right to open the menu still). **From what I can tell, this is a possible bug or support issue with Plasma Mobile and not this app**

# v1.5

- Fixed app not using user selected day/night style in Plasma Mobile

# v1.4

- Finished adding Old and New Testament footnotes

- Adjusted footnote tooltip display

- Added glossary

- Added preface

- Window now remembers last window size (added for desktop users)

# v1.3

- Added remaining books

- Added glossary

# v1.2

- Added restore to last read/scroll point

- Added easy chapter navigation/selection

- Fixed footnotes popup placement

- Reduced file count by changing css/js

- Added book loading screen/popup

# v1.1

- Added Dark and Light mode switching

- Added restore last book read

- Made CSS and JavaScript external to reduce book filesize

- Finished adding all footnotes to Genesis

# v1.0

- Initial app with Genesis and partial descriptive footnote text
