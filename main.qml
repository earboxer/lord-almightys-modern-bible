/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls.Material 2.13
import Qt.labs.settings 1.1


ApplicationWindow {
    id: window
    width: settings.appwidth
    height: settings.appheight
    visible: true
    title: qsTr(" ")
    Settings {
        id:settings
        property bool swstate: themeSwitch.checked
        property string newhomepage: "Home.qml"
        property int appwidth: 360
        property int appheight: 720
    }
    onClosing: {
        settings.appwidth = window.width
        settings.appheight = window.height
    }

    Material.theme: {
        if (themeSwitch.checked) {
            Material.primary="dimgrey"
            Material.foreground="antiquewhite"
            Material.background="dimgrey"
            Material.accent="antiquewhite"


        } else {
            Material.primary="antiquewhite"
            Material.foreground="dimgrey"
            Material.background="antiquewhite"
            Material.accent="dimgrey"

        }
    }
    header: ToolBar {
        contentHeight: toolButton.implicitHeight

        ToolButton {
            id: toolButton
            text: {
                if (themeSwitch.checked) {
                    "<font color=\"antiquewhite\">\u2630</font>"
                } else {
                    "<font color=\"dimgrey\">\u2630</font>"

                }
            }
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            onClicked: drawer.open()
        }

        Label {
            text: stackView.currentItem.title
            anchors.centerIn: parent
        }
        Switch {
            id: themeSwitch
            text: {
                if (themeSwitch.checked) {
                    "<font color=\"antiquewhite\">Night</font>"
                } else {
                    "<font color=\"dimgrey\">Night</font>"

                }
            }
            anchors.right: parent.right
            checked: settings.swstate
        }
    }
    Drawer {
        id: drawer
        width: window.width * 0.66
        height: window.height
        ScrollView {
            width: parent.width
            height : parent.height
            ScrollBar.vertical.policy: ScrollBar.AlwaysOn
            contentWidth: column.width
            contentHeight: column.height
            Column {
                width: parent.width
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                        color: {
                            if(themeSwitch.checked){
                                "antiquewhite"
                            } else {
                                "dimgrey"
                            }
                        }
                    }
                }
                Label {
                    text: qsTr("Lord Almighty's Modern Bible")
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    topPadding: 10
                    bottomPadding: 10
                }
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                        color: {
                            if(themeSwitch.checked){
                                "antiquewhite"
                            } else {
                                "dimgrey"
                            }
                        }
                    }
                }
                ItemDelegate {
                    text: qsTr("Cover")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="Home.qml"
                        stackView.replace("Home.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Preface")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Preface.qml"
                        stackView.replace("bookpage/Preface.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Glossary")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Glossary.qml"
                        stackView.replace("bookpage/Glossary.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("About")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="Home.qml"
                        stackView.replace("About.ui.qml")
                        drawer.close()
                    }
                }
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                        color: {
                            if(themeSwitch.checked){
                                "antiquewhite"
                            } else {
                                "dimgrey"
                            }
                        }
                    }
                }
                Label {
                    text: qsTr("Old Testament Books")
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    topPadding: 10
                    bottomPadding: 10
                }
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                        color: {
                            if(themeSwitch.checked){
                                "antiquewhite"
                            } else {
                                "dimgrey"
                            }
                        }
                    }
                }
                ItemDelegate {
                    text: qsTr("Genesis")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Genesis.qml"
                        stackView.replace("bookpage/Genesis.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Exodus")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Exodus.qml"
                        stackView.replace("bookpage/Exodus.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Leviticus")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Leviticus.qml"
                        stackView.replace("bookpage/Leviticus.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Numbers")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Numbers.qml"
                        stackView.replace("bookpage/Numbers.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Deuteronomy")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Deuteronomy.qml"
                        stackView.replace("bookpage/Deuteronomy.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Joshua")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Joshua.qml"
                        stackView.replace("bookpage/Joshua.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Judges")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Judges.qml"
                        stackView.replace("bookpage/Judges.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Ruth")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Ruth.qml"
                        stackView.replace("bookpage/Ruth.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Samuel")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/1Samuel.qml"
                        stackView.replace("bookpage/1Samuel.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Samuel")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/2Samuel.qml"
                        stackView.replace("bookpage/2Samuel.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Kings")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/1Kings.qml"
                        stackView.replace("bookpage/1Kings.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Kings")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/2Kings.qml"
                        stackView.replace("bookpage/2Kings.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Chronicles")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/1Chronicles.qml"
                        stackView.replace("bookpage/1Chronicles.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Chronicles")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/2Chronicles.qml"
                        stackView.replace("bookpage/2Chronicles.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Ezra")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Ezra.qml"
                        stackView.replace("bookpage/Ezra.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Nehemiah")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Nehemiah.qml"
                        stackView.replace("bookpage/Nehemiah.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Esther")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Esther.qml"
                        stackView.replace("bookpage/Esther.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Job")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Job.qml"
                        stackView.replace("bookpage/Job.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Psalms")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Psalms.qml"
                        stackView.replace("bookpage/Psalms.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Proverbs")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Proverbs.qml"
                        stackView.replace("bookpage/Proverbs.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Ecclesiastes")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Ecclesiastes.qml"
                        stackView.replace("bookpage/Ecclesiastes.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Song of Solomon")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/SongofSolomon.qml"
                        stackView.replace("bookpage/SongofSolomon.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Isaiah")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Isaiah.qml"
                        stackView.replace("bookpage/Isaiah.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Jeremiah")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Jeremiah.qml"
                        stackView.replace("bookpage/Jeremiah.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Lamentations")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Lamentations.qml"
                        stackView.replace("bookpage/Lamentations.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Ezekiel")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Ezekiel.qml"
                        stackView.replace("bookpage/Ezekiel.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Daniel")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Daniel.qml"
                        stackView.replace("bookpage/Daniel.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Hosea")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Hosea.qml"
                        stackView.replace("bookpage/Hosea.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Joel")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Joel.qml"
                        stackView.replace("bookpage/Joel.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Amos")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Amos.qml"
                        stackView.replace("bookpage/Amos.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Obadiah")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Obadiah.qml"
                        stackView.replace("bookpage/Obadiah.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Jonah")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Jonah.qml"
                        stackView.replace("bookpage/Jonah.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Micah")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Micah.qml"
                        stackView.replace("bookpage/Micah.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Nahum")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Nahum.qml"
                        stackView.replace("bookpage/Nahum.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Habakkuk")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Habakkuk.qml"
                        stackView.replace("bookpage/Habakkuk.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Zephaniah")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Zephaniah.qml"
                        stackView.replace("bookpage/Zephaniah.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Haggai")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Haggai.qml"
                        stackView.replace("bookpage/Haggai.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Zechariah")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Zechariah.qml"
                        stackView.replace("bookpage/Zechariah.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Malachi")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Malachi.qml"
                        stackView.replace("bookpage/Malachi.qml")
                        drawer.close()
                    }
                }
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                        color: {
                            if(themeSwitch.checked){
                                "antiquewhite"
                            } else {
                                "dimgrey"
                            }
                        }
                    }
                }
                Label {
                    text: qsTr("Deuterocanon Books")
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    topPadding: 10
                    bottomPadding: 10
                }
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                        color: {
                            if(themeSwitch.checked){
                                "antiquewhite"
                            } else {
                                "dimgrey"
                            }
                        }
                    }
                }
                ItemDelegate {
                    text: qsTr("Tobit")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Tobit.qml"
                        stackView.replace("bookpage/Tobit.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Judith")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Judith.qml"
                        stackView.replace("bookpage/Judith.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Esther (Greek)")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Esther-Greek.qml"
                        stackView.replace("bookpage/Esther-Greek.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Wisdom of Solomon")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Wisdom-of-Solomon.qml"
                        stackView.replace("bookpage/Wisdom-of-Solomon.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Sirach")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Sirach.qml"
                        stackView.replace("bookpage/Sirach.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Baruch")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Baruch.qml"
                        stackView.replace("bookpage/Baruch.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Maccabees")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/1Maccabees.qml"
                        stackView.replace("bookpage/1Maccabees.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Maccabees")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/2Maccabees.qml"
                        stackView.replace("bookpage/2Maccabees.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Esdras")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/1Esdras.qml"
                        stackView.replace("bookpage/1Esdras.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Prayer of Manasses")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Prayer-of-Manasses.qml"
                        stackView.replace("bookpage/Prayer-of-Manasses.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Psalm 151")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Psalm151.qml"
                        stackView.replace("bookpage/Psalm151.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("3 Maccabees")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/3Maccabees.qml"
                        stackView.replace("bookpage/3Maccabees.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Esdras")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/2Esdras.qml"
                        stackView.replace("bookpage/2Esdras.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("4 Maccabees")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/4Maccabees.qml"
                        stackView.replace("bookpage/4Maccabees.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Daniel (Greek)")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Daniel-Greek.qml"
                        stackView.replace("bookpage/Daniel-Greek.qml")
                        drawer.close()
                    }
                }
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                        color: {
                            if(themeSwitch.checked){
                                "antiquewhite"
                            } else {
                                "dimgrey"
                            }
                        }
                    }
                }
                Label {
                    text: qsTr("New Testament Books")
                    width: parent.width
                    horizontalAlignment: Text.AlignHCenter
                    topPadding: 10
                    bottomPadding: 10
                }
                ToolSeparator {
                    orientation: Qt.Horizontal
                    width: parent.width
                    contentItem: Rectangle {
                        implicitHeight: 1
                        color: {
                            if(themeSwitch.checked){
                                "antiquewhite"
                            } else {
                                "dimgrey"
                            }
                        }
                    }
                }
                ItemDelegate {
                    text: qsTr("Matthew")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Matthew.qml"
                        stackView.replace("bookpage/Matthew.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Mark")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Mark.qml"
                        stackView.replace("bookpage/Mark.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Luke")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Luke.qml"
                        stackView.replace("bookpage/Luke.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("John")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/John.qml"
                        stackView.replace("bookpage/John.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Acts")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Acts.qml"
                        stackView.replace("bookpage/Acts.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Romans")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Romans.qml"
                        stackView.replace("bookpage/Romans.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Corinthians")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/1Corinthians.qml"
                        stackView.replace("bookpage/1Corinthians.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Corinthians")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/2Corinthians.qml"
                        stackView.replace("bookpage/2Corinthians.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Galatians")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Galatians.qml"
                        stackView.replace("bookpage/Galatians.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Ephesians")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Ephesians.qml"
                        stackView.replace("bookpage/Ephesians.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Philippians")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Philippians.qml"
                        stackView.replace("bookpage/Philippians.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Colossians")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Colossians.qml"
                        stackView.replace("bookpage/Colossians.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Thessalonians")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/1Thessalonians.qml"
                        stackView.replace("bookpage/1Thessalonians.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Thessalonians")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/2Thessalonians.qml"
                        stackView.replace("bookpage/2Thessalonians.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Timothy")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/1Timothy.qml"
                        stackView.replace("bookpage/1Timothy.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Timothy")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/2Timothy.qml"
                        stackView.replace("bookpage/2Timothy.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Titus")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Titus.qml"
                        stackView.replace("bookpage/Titus.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Philemon")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Philemon.qml"
                        stackView.replace("bookpage/Philemon.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Hebrews")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Hebrews.qml"
                        stackView.replace("bookpage/Hebrews.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("James")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/James.qml"
                        stackView.replace("bookpage/James.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 Peter")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/1Peter.qml"
                        stackView.replace("bookpage/1Peter.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 Peter")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/2Peter.qml"
                        stackView.replace("bookpage/2Peter.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("1 John")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/1John.qml"
                        stackView.replace("bookpage/1John.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("2 John")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/2John.qml"
                        stackView.replace("bookpage/2John.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("3 John")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/3John.qml"
                        stackView.replace("bookpage/3John.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Jude")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Jude.qml"
                        stackView.replace("bookpage/Jude.qml")
                        drawer.close()
                    }
                }
                ItemDelegate {
                    text: qsTr("Revelation")
                    width: parent.width
                    onClicked: {
                        settings.newhomepage="bookpage/Revelation.qml"
                        stackView.replace("bookpage/Revelation.qml")
                        drawer.close()
                    }
                }
            }
        }
    }
    StackView {
        id: stackView

        initialItem: {
                stackView.replace(settings.newhomepage)
        }
        anchors.fill: parent
    }
}
