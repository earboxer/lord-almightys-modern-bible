/* 
Copyright © 2020 The3DmaN
*/

import QtQuick 2.12
import QtQuick.Controls 2.5

Page {
    id: page
    title: {
        if (themeSwitch.checked) {
            qsTr("<font color=\"antiquewhite\">About</font>")
        } else {
            qsTr("<font color=\"dimgrey\">About</font>")

        }
    }
    Column {
        id: column
        anchors.fill: parent
        anchors.margins: 20
        spacing: 5
        Image {
            id: image
            source: "files/images/LAMB.png"
            anchors.horizontalCenter: parent.horizontalCenter
            fillMode: Image.PreserveAspectFit
        }
        Label {
            id: versionlabel
            text: qsTr("v1.4")
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Label {
            id: descriptiontext
            text: "Lord Almighty's Modern Bible (LAMB) is a cross platform Bible application. It was designed with mobile in mind and provides an offline copy of the Bible in an elegant look and feel. The Bible in this app is based on the 2020 edition of the World English Bible. Neither the scripture nor the translations in Lord Almighty's Modern Bible have been modified from the World English Bible, however, there were major modifications made to the footnotes display and some text from the WEB version. While the Bible text and glossary used in this app are in the public domain, this application and its code are released under a GPL v3 license."
            horizontalAlignment: Text.AlignLeft
            wrapMode: Text.WordWrap
            anchors.horizontalCenter: parent.horizontalCenter
            textFormat: Text.RichText
            width: column.width
        }
        Label {
            id: linklabel
            text: {
                if (themeSwitch.checked) {
                    "<a href=\"https://the3dman.gitlab.io\" style='color: antiquewhite'>https://the3dman.gitlab.io</a>"
                } else {
                    "<a href=\"https://the3dman.gitlab.io\" style='color: dimgrey'>https://the3dman.gitlab.io</a>"
                }
            }
            rightInset: 0
            leftInset: 0
            anchors.horizontalCenter: parent.horizontalCenter
            textFormat: Text.RichText
            onLinkActivated: Qt.openUrlExternally(link)
        }
        Label {
            id: copyrightlabel
            text: qsTr("Copyright © 2020 The3DmaN")
            anchors.horizontalCenter: parent.horizontalCenter
        }
        ToolSeparator {
            orientation: Qt.Horizontal
            width: parent.width
            contentItem: Rectangle {
                implicitHeight: 1
                color: {
                    if(themeSwitch.checked){
                        "antiquewhite"
                    } else {
                        "dimgrey"
                    }
                }
            }
        }
        Label {
            id: linklabelweb
            text: {
                if (themeSwitch.checked) {
                    "<a href=\"https://WorldEnglish.bible\" style='color: antiquewhite'>World English Bible (Public Domain)</a>"
                } else {
                    "<a href=\"https://WorldEnglish.bible\" style='color: dimgrey'>World English Bible (Public Domain)</a>"
                }
            }
            rightInset: 0
            leftInset: 0
            anchors.horizontalCenter: parent.horizontalCenter
            textFormat: Text.RichText
            onLinkActivated: Qt.openUrlExternally(link)
        }
    }
}
